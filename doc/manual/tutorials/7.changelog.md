* 0.2.1 2021-06-04
    * Updated packages
    * Not swithced to latest typescrypt for compatibility with typedoc
    * Switched to typedoc-plugin-pages-fork as temporary fix for tutorials
      generation
* 0.2.0 2020-09-13
    * Added new support methods
* 0.1.1 2020-09-01
    * Switched to typedoc
    * Better package management
    * Updated packages
* 0.1.0 2020-03-16
    * First release
