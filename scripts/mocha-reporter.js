// my-reporter.js
const StackTraceParser = require("stacktrace-parser");
const path = require("path");
const mocha = require("mocha");


function logError(_test, err)
{
    const lines = StackTraceParser.parse(err.stack);
    // We are only interested in the place in the test which originated
    // the error:
    const line = lines.find(line => line.file.indexOf(".ts") !== -1);
    if (line)
    {
        console.log(`\n${line.file}:${line.lineNumber}:${line.column} - `
        + `error TS0000: ${err.message.split("\n")[0]}\n`);
    }
}


function MyReporter(runner)
{
    //mocha.reporters.Base.call(this, runner);
    mocha.reporters.Spec.call(this, runner);
    runner.on("fail", logError);
}


// To have this reporter "extend" a built-in reporter uncomment the
// following line:
mocha.utils.inherits(MyReporter, mocha.reporters.Spec);

module.exports = MyReporter;
