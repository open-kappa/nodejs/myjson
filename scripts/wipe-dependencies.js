const fs = require('fs');

function wipeDependencies()
{
    const file = fs.readFileSync('package.json');
    const content = JSON.parse(file);
    for (const devDep in content.devDependencies)
    {
        content.devDependencies[devDep] = '*';
        console.log(devDep);
    }
    console.log("----");
    for (const dep in content.dependencies)
    {
        content.dependencies[dep] = '*';
        console.log(dep);
    }
    fs.writeFileSync('package.json', JSON.stringify(content, null, 4) + "\n");
}

if (require.main === module)
{
    wipeDependencies()
}
else
{
    module.exports = wipeDependencies
}
