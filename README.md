# @open-kappa/myjson
This package provides a simple utility library, to manage JSON.
It supports also *typescript*.

# Links

 * Project homepage: [hosted on GitLab Pages](
   https://open-kappa.gitlab.io/nodejs/myjson)

 * Project sources: [hosted on gitlab.com](
   https://gitlab.com/open-kappa/nodejs/myjson)

 * List of open-kappa projects, [hosted on GitLab Pages](
   https://open-kappa.gitlab.io)

# License

*@open-kappa/myjson* is released under the liberal MIT License.
Please refer to the LICENSE.txt project file for further details.

# Patrons

This node-red module has been sponsored by [Gizero Energie s.r.l.](
www.gizeroenergie.it).
