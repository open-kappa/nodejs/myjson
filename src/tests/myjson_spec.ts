import "should";
import {
    MyJson,
    MyJsonArray,
    MyJsonFixed,
    MyJsonMap,
    MyJsonValue
} from "../myjson/index";
import {MyTest} from "@open-kappa/mytest";

class ThisTest
    extends MyTest
{
    private jsonTree: MyJson | null;
    private readonly json: any;

    constructor()
    {
        super("MyJson tests");
        this.jsonTree = null;
        this.json = {
            "arrayIntValues": [
                0,
                1
            ],
            "intMapValues": {
                "first": 0,
                "second": 1
            }
        };
    }

    registerTests(): void
    {
        const self = this;
        self.registerTest("build", self._testBuild);
        self.registerTest("parse fail", self._testFailingParsing, true);
        self.registerTest("parse ok", self._testParsing);
        self.registerTest("toJSON", self._testToJson);
        self.registerTest("clone", self._testClone);
    }

    private _testBuild(): void
    {
        const self = this;
        const map = new MyJsonMap<MyJsonValue<number>>(
            new MyJsonValue<number>(1, false, "optional"),
            true,
            "intMapValues"
        );
        const array = new MyJsonArray<MyJsonValue<number>>(
            new MyJsonValue<number>(1, false, "optional"),
            false,
            "arrayIntValues"
        );
        const root = new MyJsonFixed(
            true,
            "root"
        );
        root.set(array.getJsonName(), array);
        root.set(map.getJsonName(), map);
        self.jsonTree = root;
    }

    private _testFailingParsing(): void
    {
        const self = this;
        const json = {
            "arrayIntValues": [
                0,
                1
            ],
            "intMapValues": {
                "first": 0,
                "second": "noooo!"
            }
        };
        const tree = self.jsonTree as MyJson;
        tree.parseJson(json);
    }

    private _testParsing(): void
    {
        const self = this;
        const tree = self.jsonTree as MyJson;
        tree.parseJson(self.json);
    }

    private _testToJson(): void
    {
        const self = this;
        const treeString = JSON.stringify(self.jsonTree);
        const jsonString = JSON.stringify(self.json);
        // expect(
        //     treeString === jsonString,
        //     "Failed equality:"
        //     + " obj: " + treeString
        //     + " json: " + jsonString
        // ).to.be.true;
        (treeString === jsonString).should.be.true(
            "Failed equality:"
            + " obj: " + treeString
            + " json: " + jsonString
        );
    }

    private _testClone(): void
    {
        const self = this;
        const jsonTree = self.jsonTree as MyJson;
        const treeString = JSON.stringify(jsonTree.clone());
        const jsonString = JSON.stringify(self.json);
        // expect(
        //     treeString === jsonString,
        //     "Failed equality:"
        //     + " obj: " + treeString
        //     + " json: " + jsonString
        // ).to.be.true;
        (treeString === jsonString).should.be.true(
            "Failed equality:"
            + " obj: " + treeString
            + " json: " + jsonString
        );
    }
}

const test = new ThisTest();
test.run();
