import {
    MyJson,
    MyJsonArray,
    MyJsonFixed,
    MyJsonFlex,
    MyJsonMap,
    MyJsonValue
} from "./myjsonImpl";


/**
 * @brief Base class for classes mapping JSON objects.
 */
abstract class MyJsonBaseObject
    extends MyJson
{
    /**
     * @brief Constructor.
     * @param {boolean} isMandatory True if the object must appear in the
     *     hierarchy.
     * @param {string} name The name of the object.
     */
    constructor(
        isMandatory: boolean,
        name: string
    )
    {
        super(isMandatory, name);
    }

    /**
     * @brief Common implementation of clone().
     * @param {MyJsonBaseObject} other The copy.
     */
    protected _cloneImpl(other: MyJsonBaseObject): void
    {
        const self = this;
        function insert(key: string, value: MyJson): boolean
        {
            other.set(key, value.clone());
            return false;
        }
        self._forEachImpl(insert);
    }

    /**
     * @brief Common implementation of isEqual().
     * @param {MyJson} other The other object of comparison.
     * @return {boolean} True if they are equals.
     */
    protected _isEqualImpl(other: MyJsonBaseObject): boolean
    {
        const self = this;
        if (self.getSize() !== other.getSize()) return false;
        let ret = true;
        function doCheck(key: string, value: MyJson): boolean
        {
            const otherValue = other.get(key);
            ret = value.isEqual(otherValue);
            return !ret;
        }
        self._forEachImpl(doCheck);
        return ret;
    }

    /**
     * @brief Check whether the object is empty.
     * @return {boolean} True if empty.
     */
    isEmpty(): boolean
    {
        return this.getSize() === 0;
    }

    /**
     * @brief Get the number of stored elements.
     * @return {number} The array size.
     */
    getSize(): number
    {
        return Object.keys(this).length;
    }

    /**
     * @brief Get the element with the given key.
     * @param {string} key The key.
     * @return {MyJson} The internal element.
     */
    get(key: string): MyJson
    {
        const self = this;
        const anySelf: any = self;
        if (!self.has(key))
        {
            self._throwValidatorError(
                "MyJsonBaseObject.get(): key not found: " + key
            );
        }
        return anySelf[key];
    }

    /**
     * @brief Set the element with given key.
     * @param {key} key The key.
     * @param {MyJson} value The element.
     */
    set(key: string, value: MyJson): void
    {
        const self = this;
        const anySelf: any = self;
        value.setJsonName(key);
        // @todo should check validity...
        // @todo add a generic json object...
        // if (self.has(key) && self.get(key).isMandatory())
        // {
        //     const el = self.get(key);
        // }
        // else
        // {
        //     const el = self._getValidatorElement();
        // }
        anySelf[key] = value;
    }

    /**
     * @brief Check if the element exists.
     * @param {string} key The key.
     * @return {boolean} True if it exists.
     */
    has(key: string): boolean
    {
        const self = this;
        return key in self;
    }

    /**
     * @brief Common implementation of forEach()..
     * The callback takes the key, the element, and the whole object.
     * It returns true to break the loop before having rolled on all elements.
     * @param {(key:string,value:MyJson,obj:MyJsonBaseObject)=>boolean} func
     *     The callback.
     * @param {any | null | undefined} [funcThis=null] The optional "this" for
     *     the callback.
     */
    protected _forEachImpl(
        func: (
            key: string,
            value: MyJson,
            obj: MyJsonBaseObject
        ) => boolean,
        funcThis: any | null | undefined = null
    ): boolean
    {
        const self = this;
        const keys = Object.keys(self);
        let ret = false;
        for (let i = 0; i < keys.length; ++i)
        {
            const key = keys[i];
            const value = self.get(key);
            ret = func.apply(funcThis, [key, value, self]);
            if (ret) break;
        }
        return ret;
    }

    /**
     * @brief Common implementation of merge().
     * Already existent keys are merged recursively.
     * @param {MyJsonBaseObject} other The other object to merge.
     * @throws {Error} If the two objects cannot be merged.
     */
    protected _mergeImpl(other: MyJsonBaseObject): void
    {
        const self = this;
        function doMerge(key: string, otherValue: MyJson): boolean
        {
            if (self.has(key))
            {
                const value = self.get(key);
                value.merge(otherValue);
            }
            else
            {
                self.set(key, otherValue.clone());
            }
            return false;
        }
        other._forEachImpl(doMerge);
    }

    /**
     * @brief Add a new child element.
     * This is a build-time support method.
     * @param {MyJson} element The child element.
     * @return {MyJson} This.
     * @throws {Error} If element cannot be added.
     */
    add(element: MyJson): MyJsonBaseObject
    {
        const self = this;
        self.set(element.getJsonName(), element);
        return self;
    }

    /**
     * @brief Get element as array.
     * @param {string} key The key.
     * @throws {Error} If element is not an array.
     */
    getAsArray<Element extends MyJson>(key: string): MyJsonArray<Element>
    {
        const self = this;
        const ret = self.get(key);
        if (!(ret instanceof MyJsonArray))
        {
            self._throwValidatorError("Not a MyJsonArray");
        }
        // @todo add template check
        return ret as MyJsonArray<Element>;
    }

    /* eslint-disable @typescript-eslint/no-unused-vars */
    /**
     * @brief Get element as fixed.
     * @param {string} key The key.
     * @throws {Error} If element is not a fixed.
     */
    getAsFixed<Element extends MyJson>(key: string): MyJsonFixed
    {
        /* eslint-enable @typescript-eslint/no-unused-vars */
        const self = this;
        const ret = self.get(key);
        if (!(ret instanceof MyJsonFixed))
        {
            self._throwValidatorError("Not a MyJsonFixed");
        }
        // @todo add template check
        return ret as MyJsonFixed;
    }

    /**
     * @brief Get element as flex.
     * @param {string} key The key.
     * @throws {Error} If element is not a flex.
     */
    getAsFlex<Element extends MyJson>(key: string): MyJsonFlex<Element>
    {
        const self = this;
        const ret = self.get(key);
        if (!(ret instanceof MyJsonFlex))
        {
            self._throwValidatorError("Not a MyJsonFlex");
        }
        // @todo add template check
        return ret as MyJsonFlex<Element>;
    }

    /**
     * @brief Get element as map.
     * @param {string} key The key.
     * @throws {Error} If element is not a map.
     */
    getAsMap<Element extends MyJson>(key: string): MyJsonMap<Element>
    {
        const self = this;
        const ret = self.get(key);
        if (!(ret instanceof MyJsonMap))
        {
            self._throwValidatorError("Not a MyJsonMap");
        }
        // @todo add template check
        return ret as MyJsonMap<Element>;
    }

    /**
     * @brief Get element as value.
     * @param {string} key The key.
     * @throws {Error} If element is not a value.
     */
    getAsValue<Element>(key: string): MyJsonValue<Element>
    {
        const self = this;
        const ret = self.get(key);
        if (!(ret instanceof MyJsonValue))
        {
            self._throwValidatorError("Not a MyJsonValue");
        }
        // @todo add template check
        return ret as MyJsonValue<Element>;
    }

    /**
     * @brief Get element as value.
     * @param {string} key The key.
     * @throws {Error} If element is not a value.
     */
    getAsStringValue(key: string): MyJsonValue<string>
    {
        return this.getAsValue<string>(key);
    }

    /**
     * @brief Get element as value.
     * @param {string} key The key.
     * @throws {Error} If element is not a value.
     */
    getAsNumberValue(key: string): MyJsonValue<number>
    {
        return this.getAsValue<number>(key);
    }

    /**
     * @brief Get element as value.
     * @param {string} key The key.
     * @throws {Error} If element is not a value.
     */
    getAsBooleanValue(key: string): MyJsonValue<boolean>
    {
        return this.getAsValue<boolean>(key);
    }
}


export {
    MyJsonBaseObject
};
export default MyJsonBaseObject;
