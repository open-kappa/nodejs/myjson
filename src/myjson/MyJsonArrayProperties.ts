/**
 * @brief Hidden properties of MyJsonArray.
 */
class MyJsonArrayProperties<Element>
{
    /** The validator element. */
    element: Element;

    /** The actual array of elements. */
    values: Array<Element>;

    /**
     * @brief Constructor.
     * @param {Element} element The validator element.
     */
    constructor(element: Element)
    {
        this.element = element;
        this.values = [];
    }
}


export {
    MyJsonArrayProperties
};

export default MyJsonArrayProperties;
