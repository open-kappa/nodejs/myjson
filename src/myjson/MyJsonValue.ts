import {MyJson} from "./myjsonImpl";


/**
 * @brief Holder for generic values.
 * Usually it is the leaf ot a JSON tree.
 * @typeparam Value The type of the stored value.
 */
class MyJsonValue<Value>
    extends MyJson
{
    /** The stored value. */
    private value: Value;

    /**
     * @brief Constructor.
     * @param {Value} value The default value.
     * @param {boolean} isMandatory True if the object must appear in the
     *     hierarchy.
     * @param {string} name The name of the object.
     */
    constructor(
        value: Value,
        isMandatory: boolean,
        name: string
    )
    {
        super(isMandatory, name);
        this.value = value;
    }

    /**
     * @brief Get the value.
     * @return {Value} The value.
     */
    getValue(): Value
    {
        return this.value;
    }

    /**
     * @brief Set the value.
     * @param {Value} value The value.
     */
    setValue(value: Value): void
    {
        this.value = value;
    }

    /**
     * @brief Parse a JSON object.
     * @param {any} json The JSON to parse.
     * @throws {Error} If a validation error occurrs.
     */
    protected parseJsonImpl(json: any): void
    {
        const self = this;
        const value = JSON.parse(JSON.stringify(json));
        if (typeof self.value !== typeof value)
        {
            self._throwValidatorError(
                "No matching value types:"
                + " expected: " + typeof self.value
                + " found: " + typeof value
            );
        }
        self.value = value;
    }

    /**
     * @brief Clone this object.
     * @return {MyJsonValue<Value>} The copy.
     */
    clone(): MyJsonValue<Value>
    {
        const self = this;
        let newValue: any = null;
        const oldValue: any = self.value;
        if (typeof oldValue.clone === "function")
        {
            newValue = oldValue.clone();
        }
        else
        {
            newValue = JSON.parse(JSON.stringify(oldValue));
        }
        const ret = new MyJsonValue<Value>(
            newValue,
            self.isMandatory(),
            self.getJsonName()
        );
        return ret;
    }

    /**
     * @brief Check if two JSON objects are equals.
     * @param {MyJson} other The other object of comparison.
     * @return {boolean} True if they are equals.
     */
    isEqual(other: MyJson): boolean
    {
        const self = this;
        if (!(other instanceof MyJsonValue)) return false;
        const value: any = self.getValue();
        const otherValue = other.getValue();
        if (typeof value !== typeof otherValue) return false;
        if (typeof value.isEqual === "function")
        {
            return value.isEqual(otherValue);
        }
        return value === otherValue;
    }

    /**
     * @brief Map to JSON.
     * @return {Value} The value.
     */
    toJSON(): Value
    {
        const self = this;
        return self.value;
    }

    /**
     * @brief Clear the object content.
     */
    protected clearImpl(): void
    {
        const self = this;
        const tmp: any = self.value;
        if (typeof tmp.clear !== "function") return;
        tmp.clear();
    }

    /**
     * @brief Merge the two objects.
     * The values are replaced.
     * @param {MyJson} other The other object to merge.
     * @throws {Error} If the two objects cannot be merged.
     */
    merge(other: MyJson): void
    {
        const self = this;
        if (!(other instanceof MyJsonValue))
        {
            self._throwValidatorError("Invalid other instance.");
        }
        const otherObj = other as MyJsonValue<Value>;
        const otherValue: any = otherObj.value;
        const tmp: any = self.value;
        if (typeof tmp.merge === "function")
        {
            tmp.merge(otherObj.value);
        }
        else if (typeof otherValue.clone === "function")
        {
            self.setValue(otherValue.clone());
        }
        else
        {
            self.setValue(JSON.parse(JSON.stringify(otherValue)));
        }
    }

    /**
     * @brief Add a new child element.
     * This is a build-time support method.
     * @param {MyJson} _element The child element.
     * @return {MyJson} This.
     * @throws {Error} If element cannot be added.
     */
    add(_element: MyJson): MyJson
    {
        const self = this;
        self._throwValidatorError("Cannot add elements to MyJsonValue.");
        return self;
    }
}

export {
    MyJsonValue
};

export default MyJsonValue;
