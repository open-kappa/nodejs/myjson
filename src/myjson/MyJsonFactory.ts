/* eslint-disable class-methods-use-this */
import {
    MyJson,
    MyJsonArray,
    MyJsonFixed,
    MyJsonFlex,
    MyJsonMap,
    MyJsonValue
} from "./myjsonImpl";


/**
 * @brief Support factory class.
 */
class MyJsonFactory
{
    /**
     * @brief Constructor.
     */
    constructor()
    {
        // ntd
    }

    /**
     * @brief Build an array object.
     * @typeparam Element The element of the object.
     * @param {Element} element The validator element.
     * @param {boolean} isMandatory Whether the property is mandatory.
     * @param {string} name The property name.
     * @return {MyJsonArray<Element>} The built property.
     */
    makeArray<Element extends MyJson>(
        element: Element,
        isMandatory: boolean,
        name: string
    ): MyJsonArray<Element>
    {
        return new MyJsonArray<Element>(
            element,
            isMandatory,
            name
        );
    }

    /**
     * @brief Build a fixed object.
     * @param {boolean} isMandatory Whether the property is mandatory.
     * @param {string} name The property name.
     * @param {Array<MyJson>} elementsList The list of properties to add.
     * @return {MyJsonFixed} The built property.
     */
    makeFixed(
        isMandatory: boolean,
        name: string,
        elementsList: Array<MyJson> = []
    ): MyJsonFixed
    {
        const ret = new MyJsonFixed(
            isMandatory,
            name
        );
        function doInsert(el: MyJson): void
        {
            ret.add(el);
        }
        elementsList.forEach(doInsert);
        return ret;
    }

    /**
     * @brief Build a flex object.
     * @typeparam Element The element of the object.
     * @param {Element} element The validator element.
     * @param {boolean} isMandatory Whether the property is mandatory.
     * @param {string} name The property name.
     * @param {Array<MyJson>} elementsList The list of properties to add.
     * @return {MyJsonFlex<Element>} The built property.
     */
    makeFlex<Element extends MyJson>(
        element: Element,
        isMandatory: boolean,
        name: string,
        elementsList: Array<MyJson> = []
    ): MyJsonFlex<Element>
    {
        const ret = new MyJsonFlex<Element>(
            element,
            isMandatory,
            name
        );
        function doInsert(el: MyJson): void
        {
            ret.add(el);
        }
        elementsList.forEach(doInsert);
        return ret;
    }

    /**
     * @brief Build a map object.
     * @typeparam Element The element of the object.
     * @param {Element} element The validator element.
     * @param {boolean} isMandatory Whether the property is mandatory.
     * @param {string} name The property name.
     * @return {MyJsonMap<Element>} The built property.
     */
    makeMap<Element extends MyJson>(
        element: Element,
        isMandatory: boolean,
        name: string
    ): MyJsonMap<Element>
    {
        return new MyJsonMap<Element>(
            element,
            isMandatory,
            name
        );
    }

    /**
     * @brief Build a value object.
     * @typeparam Element The element of the value.
     * @param {Element} element The defaut value.
     * @param {boolean} isMandatory Whether the property is mandatory.
     * @param {string} name The property name.
     * @return {MyJsonValue<Element>} The built property.
     */
    makeValue<Element>(
        element: Element,
        isMandatory: boolean,
        name: string
    ): MyJsonValue<Element>
    {
        return new MyJsonValue<Element>(
            element,
            isMandatory,
            name
        );
    }

    /**
     * @brief Build a value string object.
     * @param {boolean} isMandatory Whether the property is mandatory.
     * @param {string} name The property name.
     * @param {string} [value=""] The defaut value.
     * @return {MyJsonValue<string>} The built property.
     */
    makeString(
        isMandatory: boolean,
        name: string,
        value = ""
    ): MyJsonValue<string>
    {
        return this.makeValue<string>(
            value,
            isMandatory,
            name
        );
    }

    /**
     * @brief Build a value number object.
     * @param {boolean} isMandatory Whether the property is mandatory.
     * @param {string} name The property name.
     * @param {string} [value=0] The defaut value.
     * @return {MyJsonValue<number>} The built property.
     */
    makeNumber(
        isMandatory: boolean,
        name: string,
        value = 0
    ): MyJsonValue<number>
    {
        return this.makeValue<number>(
            value,
            isMandatory,
            name
        );
    }

    /**
     * @brief Build a value string object.
     * @param {boolean} isMandatory Whether the property is mandatory.
     * @param {string} name The property name.
     * @param {boolean} [value=false] The defaut value.
     * @return {MyJsonValue<boolean>} The built property.
     */
    makeBoolean(
        isMandatory: boolean,
        name: string,
        value = false
    ): MyJsonValue<boolean>
    {
        return this.makeValue<boolean>(
            value,
            isMandatory,
            name
        );
    }

    /**
     * @brief Create and return a enum constraint.
     * @typeparam Element The type of enum values.
     * @param {Array<Element>} values The enum values.
     * @return {(ref: MyJson) => string} The contraint method.
     */
    getEnumConstraint<Element>(values: Array<Element>): (ref: MyJson) => string
    {
        function constraint(ref: MyJson): string
        {
            if (!(ref instanceof MyJsonValue))
            {
                return "Enum constrain not applyed to MyJsonValue";
            }
            const refValue = ref as MyJsonValue<Element>;
            if (values.indexOf(refValue.getValue()) === -1)
            {
                return "Invalid enum value: " + refValue.getValue();
            }
            return "";
        }
        return constraint;
    }

    /**
     * @brief Create and return a field constraint.
     * If the first field exists and holds the given value, then the fields in
     * the list must exists.
     * @typeparam Element The type of the value.
     * @param {string} field1 The field to check.
     * @param {Element | null} value1 The reference value. If null, the value is
     *     not checked.
     * @param {Array<string>} fieldsList The fields to check.
     * @return {(ref: MyJson) => string} The contraint method.
     */
    getFieldConstraint<Element>(
        field1: string,
        value1: Element | null,
        fieldsList: Array<string>
    ): (ref: MyJson) => string
    {
        function constraint(ref: MyJson): string
        {
            let obj1: any = null;
            let has2 = true;
            if (ref instanceof MyJsonFixed)
            {
                const obj = ref as MyJsonFixed;
                if (obj.has(field1)) obj1 = obj.get(field1);
                // eslint-disable-next-line no-inner-declarations
                function doHas(f2: string): void
                {
                    has2 = has2 && obj.has(f2) && obj.get(f2).isInitialized();
                }
                fieldsList.forEach(doHas);
            }
            else if (ref instanceof MyJsonFlex)
            {
                const obj = ref as MyJsonFlex<MyJson>;
                if (obj.has(field1)) obj1 = obj.get(field1);
                // eslint-disable-next-line no-inner-declarations
                function doHas(f2: string): void
                {
                    has2 = has2 && obj.has(f2) && obj.get(f2).isInitialized();
                }
                fieldsList.forEach(doHas);
            }
            else if (ref instanceof MyJsonMap)
            {
                const obj = ref as MyJsonMap<MyJson>;
                if (obj.has(field1)) obj1 = obj.get(field1);
                // eslint-disable-next-line no-inner-declarations
                function doHas(f2: string): void
                {
                    has2 = has2 && obj.has(f2) && obj.get(f2).isInitialized();
                }
                fieldsList.forEach(doHas);
            }
            else
            {
                return "Field constrain applyed to invalid object";
            }
            if (obj1 === null) return "";
            if (value1 !== null)
            {
                if (!(obj1 instanceof MyJsonValue))
                {
                    return "Invalid field matching object.";
                }
                const f1 = obj1 as MyJsonValue<Element>;
                if (f1.getValue() !== value1)
                {
                    return "";
                }
            }
            if (!has2)
            {
                return "Missing required field: " + fieldsList;
            }
            return "";
        }
        return constraint;
    }
}


export {
    MyJsonFactory
};

export default MyJsonFactory;
