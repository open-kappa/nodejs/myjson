import {
    MyJson,
    MyJsonArrayProperties
} from "./myjsonImpl";

/** Key for symbol name. */
const KEY_NAME = "_okmj_array_impl";


// eslint-disable-next-line max-len
// @todo https://stackoverflow.com/questions/37714787/can-i-extend-proxy-with-an-es2015-class
/**
 * @brief A JSON array of homogeneous objects.
 * @typeparam Element The type of the array elements.
 */
class MyJsonArray<Element extends MyJson>
    extends MyJson
{
    /**
     * @brief Constructor.
     * @param {Element} element The validator for array elements. If it is
     *     mandatory, then the array cannot be empty.
     * @param {boolean} isMandatory True if the object must appear in the
     *     hierarchy.
     * @param {string} name The name of the object.
     */
    constructor(
        element: Element,
        isMandatory: boolean,
        name: string
    )
    {
        super(isMandatory, name);
        const anySelf: any = this;
        anySelf[Symbol.for(KEY_NAME)] =
            new MyJsonArrayProperties(element);
    }

    /**
     * @brief Return the internal validator element of the array.
     * It uses a symbol to hide the properties to "usual" javascript methods.
     * @private
     * @return {Element} The validator.
     */
    private _getValidatorElement(): Element
    {
        const anySelf: any = this;
        return anySelf[Symbol.for(KEY_NAME)].element;
    }

    /**
     * @brief Return the internal array of objects.
     * It uses a symbol to hide the properties to "usual" javascript methods.
     * @private
     * @return {Element} The validator.
     */
    private _getValidatorValues(): Array<Element>
    {
        const anySelf: any = this;
        return anySelf[Symbol.for(KEY_NAME)].values;
    }

    /**
     * @brief Parse a JSON object.
     * @param {any} json The JSON to parse.
     * @throws {Error} If a validation error occurrs.
     */
    protected parseJsonImpl(json: any): void
    {
        const self = this;
        if (!Array.isArray(json))
        {
            const msg = "Expected JSON array, but found "
                + JSON.stringify(json);
            self._throwValidatorError(msg);
        }

        const element = self._getValidatorElement();
        if (element.isMandatory()
            && json.length === 0)
        {
            const msg = "Array cannot be empty.";
            self._throwValidatorError(msg);
        }

        for (let i = 0; i < json.length; ++i)
        {
            const jsonValue = json[i];
            const newElement = element.clone() as Element;
            newElement.parseJson(jsonValue);
            self.push(newElement);
        }
    }

    /**
     * @brief Clone this object.
     * @return {MyJsonArray<Element>} The copy.
     */
    clone(): MyJsonArray<Element>
    {
        const self = this;
        const ret = new MyJsonArray<Element>(
            self._getValidatorElement().clone() as Element,
            self.isMandatory(),
            self.getJsonName()
        );
        function insert(el: Element): boolean
        {
            ret.push(el.clone() as Element);
            return false;
        }
        self.forEach(insert);
        return ret;
    }

    /**
     * @brief Check if two JSON objects are equals.
     * @param {MyJson} other The other object of comparison.
     * @return {boolean} True if they are equals.
     */
    isEqual(other: MyJson): boolean
    {
        const self = this;
        if (!(other instanceof MyJsonArray)) return false;
        const otherArray = other;
        if (self.getSize() !== other.getSize()) return false;
        let ret = true;
        function doCheck(el: Element, index: number): boolean
        {
            const otherEl = otherArray.get(index);
            ret = el.isEqual(otherEl);
            return !ret;
        }
        self.forEach(doCheck);
        return ret;
    }

    /**
     * @brief Check whether the array is empty.
     * @return {boolean} True if empty.
     */
    isEmpty(): boolean
    {
        return this.getSize() === 0;
    }

    /**
     * @brief Get the number of stored elements.
     * @return {number} The array size.
     */
    getSize(): number
    {
        return this._getValidatorValues().length;
    }

    /**
     * @brief Get the element at given position.
     * @param {number} pos The position.
     * @return {Element} The internal element.
     */
    get(pos: number): Element
    {
        return this._getValidatorValues()[pos];
    }

    /**
     * @brief Set the element at given position.
     * @param {number} pos The position.
     * @param {Element} value The element.
     */
    set(pos: number, value: Element): void
    {
        this._getValidatorValues()[pos] = value;
    }

    /**
     * @brief Push back a new element into the array.
     * @param {Element} value The new element.
     */
    push(value: Element): void
    {
        this._getValidatorValues().push(value);
    }


    /**
     * @brief Clear the object content.
     */
    protected clearImpl(): void
    {
        this._getValidatorValues().splice(0);
    }

    /**
     * @brief Execute the given callback on each array element.
     * The callback takes the element, its index, and the whole array.
     * It returns true to break the loop before having rolled on all elements.
     * @param {(value:Element,index:number,array:Array<Element>)=>boolean} func
     *     The callback.
     * @param {any | null | undefined} [funcThis=null] The optional "this" for
     *     the callback.
     */
    forEach(
        func: (value: Element, index: number, array: Array<Element>) => boolean,
        funcThis: any | null | undefined = null
    ): boolean
    {
        const values = this._getValidatorValues();
        let ret = false;
        for (let i = 0; i < values.length; ++i)
        {
            const element = values[i];
            ret = func.apply(funcThis, [element, i, values]);
            if (ret) break;
        }
        return ret;
    }

    /**
     * @brief Convert the object to a JSON object.
     * @return {Array<Element>} The converted object.
     */
    toJSON(): Array<Element>
    {
        const self = this;
        return self._getValidatorValues();
    }

    /**
     * @brief Merge the two objects.
     * Array elements are concatenated.
     * @param {MyJson} other The other object to merge.
     * @throws {Error} If the two objects cannot be merged.
     */
    merge(other: MyJson): void
    {
        const self = this;
        if (!(other instanceof MyJsonArray))
        {
            self._throwValidatorError("Invalid other instance.");
        }
        const otherArray = other as MyJsonArray<Element>;
        if (!self._getValidatorElement().isEqual(
            otherArray._getValidatorElement()
        ))
        {
            self._throwValidatorError("Invalid other type instance.");
        }
        const elements = self._getValidatorValues();
        function doCopy(value: Element): boolean
        {
            elements.push(value.clone() as Element);
            return false;
        }
        otherArray.forEach(doCopy);
    }

    /**
     * @brief Add a new child element.
     * This is a build-time support method.
     * @param {MyJson} _element The child element.
     * @return {MyJson} This.
     * @throws {Error} If element cannot be added.
     */
    add(_element: MyJson): MyJson
    {
        const self = this;
        self._throwValidatorError("Cannot add elements to MyJsonArray.");
        return self;
    }
}


export {
    MyJsonArray
};

export default MyJsonArray;
