import {
    MyJson,
    MyJsonBaseObject
} from "./myjsonImpl";


/**
 * @brief A generic JSON object, with a fixed set of elements.
 */
class MyJsonFixed
    extends MyJsonBaseObject
{
    /**
     * @brief Constructor.
     * @param {boolean} isMandatory True if the object must appear in the
     *     hierarchy.
     * @param {string} name The name of the object.
     */
    constructor(
        isMandatory: boolean,
        name: string
    )
    {
        super(isMandatory, name);
    }

    /**
     * @brief Parse a JSON object.
     * @param {any} json The JSON to parse.
     * @throws {Error} If a validation error occurrs.
     */
    protected parseJsonImpl(json: any): void
    {
        const self = this;
        self._validateJsonValues(json);
        self._ensureRequiredValues(json);
    }

    /**
     * @brief Clear the object content.
     */
    protected clearImpl(): void
    {
        function doClear(_key: string, obj: MyJson): boolean
        {
            obj.clear();
            return false;
        }
        const self = this;
        self.forEach(doClear);
    }

    /**
     * @brief Clone this object.
     * @return {MyJsonFixed} The copy.
     */
    clone(): MyJsonFixed
    {
        const self = this;
        const ret = new MyJsonFixed(
            self.isMandatory(),
            self.getJsonName()
        );
        self._cloneImpl(ret);
        return ret;
    }

    /**
     * @brief Check if two JSON objects are equals.
     * @param {MyJson} other The other object of comparison.
     * @return {boolean} True if they are equals.
     */
    isEqual(other: MyJson): boolean
    {
        const self = this;
        if (!(other instanceof MyJsonFixed)) return false;
        return self._isEqualImpl(other);
    }

    /**
     * @brief Parse and validate internal elements.
     * @private
     * @param {any} json The json to parse.
     * @throws {Error} If the parsing fails.
     */
    private _validateJsonValues(json: any): void
    {
        const self = this;
        const anySelf: any = self;
        const keys = Object.keys(json);
        for (let i = 0; i < keys.length; ++i)
        {
            const key = keys[i];
            const jsonValue = json[key];
            const refValue: MyJson | undefined = anySelf[key];
            if (typeof refValue === "undefined")
            {
                const msg = "Unknown property " + key;
                self._throwValidatorError(msg);
                return;
            }
            refValue.parseJson(jsonValue);
        }
    }

    /**
     * @brief Ensure required fproperties exist.
     * @private
     * @param {any} json The json to parse.
     * @throws {Error} If the validation fails.
     */
    private _ensureRequiredValues(json: any): void
    {
        const self = this;
        const anySelf: any = self;
        const keys = Object.keys(self);
        for (let i = 0; i < keys.length; ++i)
        {
            const key = keys[i];
            const jsonValue = json[key];
            const refValue: MyJson | any = anySelf[key];
            if (!(refValue instanceof MyJson))
            {
                self._throwValidatorError(
                    "Internal bug: " + refValue
                );
                return;
            }
            if (!refValue.isMandatory()) continue;
            if (typeof jsonValue === "undefined")
            {
                const msg = "Missing mandatory property " + key;
                self._throwValidatorError(msg);
            }
        }
    }

    /**
     * @brief Execute the given callback on each element.
     * The callback takes the key, the element, and the whole object.
     * It returns true to break the loop before having rolled on all elements.
     * @param {(key:string,value:MyJson,obj:MyJsonFixed)=>boolean} func
     *     The callback.
     * @param {any | null | undefined} [funcThis=null] The optional "this" for
     *     the callback.
     */
    forEach(
        func: (
            key: string,
            value: MyJson,
            obj: MyJsonFixed
        ) => boolean,
        funcThis: any | null | undefined = null
    ): boolean
    {
        const self = this;
        return self._forEachImpl(func as any, funcThis);
    }

    /**
     * @brief Merge the two objects.
     * Already existent keys are merged recursively.
     * @param {MyJson} other The other object to merge.
     * @throws {Error} If the two objects cannot be merged.
     */
    merge(other: MyJson): void
    {
        const self = this;
        if (!(other instanceof MyJsonFixed))
        {
            self._throwValidatorError("Invalid other instance.");
        }
        const otherObj = other as MyJsonFixed;
        self._mergeImpl(otherObj);
    }

    /**
     * @brief Add a new child element.
     * This is a build-time support method.
     * @param {MyJson} element The child element.
     * @return {MyJson} This.
     * @throws {Error} If element cannot be added.
     */
    add(element: MyJson): MyJsonFixed
    {
        super.add(element);
        return this;
    }
}


export {
    MyJsonFixed
};

export default MyJsonFixed;
