import stripJsonComments from "strip-json-comments";


/**
 * @brief Parse a string as a JSON object.
 * The string can contain comments.
 * @param {string} jsonString The string.
 * @return {any} The parsed JSON object.
 * @throws {Error} In case of invalid JSON string.
 */
function parseJsonString(jsonString: string): any
{
    return JSON.parse(stripJsonComments(jsonString));
}


export {
    parseJsonString
};
