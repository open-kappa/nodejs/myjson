import {
    MyJson,
    MyJsonBaseObject
} from "./myjsonImpl";

/** Key for symbol name. */
const KEY_NAME = "_okmj_map_impl";


/**
 * @brief An object representing a map from strings to objects.
 * @typeparam Element The map value type.
 */
class MyJsonMap<Element extends MyJson>
    extends MyJsonBaseObject
{
    /**
     * @brief Constructor.
     * @param {Element} element The validator for elements. If it is
     *     mandatory, then the map cannot be empty.
     * @param {boolean} isMandatory True if the object must appear in the
     *     hierarchy.
     * @param {string} name The name of the object.
     */
    constructor(
        element: Element,
        isMandatory: boolean,
        name: string
    )
    {
        super(isMandatory, name);
        const anySelf: any = this;
        anySelf[Symbol.for(KEY_NAME)] = element;
    }

    /**
     * @brief Return the internal validator element of the map.
     * It uses a symbol to hide the properties to "usual" javascript methods.
     * @private
     * @return {Element} The validator.
     */
    private _getValidatorElement(): Element
    {
        const anySelf: any = this;
        return anySelf[Symbol.for(KEY_NAME)];
    }

    /**
     * @brief Parse a JSON object.
     * @param {any} json The JSON to parse.
     * @throws {Error} If a validation error occurrs.
     */
    protected parseJsonImpl(json: any): void
    {
        const self = this;
        const keys = Object.keys(json);
        const element = self._getValidatorElement();
        if (element.isMandatory() && keys.length === 0)
        {
            const msg = "Map cannot be empty.";
            self._throwValidatorError(msg);
        }
        for (let i = 0; i < keys.length; ++i)
        {
            const key = keys[i];
            if (self.has(key))
            {
                self._throwValidatorError(
                    "MyJsonMap.parseJson(): duplicated key " + key
                );
            }
            const jsonValue = json[key];
            const newElement = element.clone() as Element;
            newElement.parseJson(jsonValue);
            self.set(key, newElement);
        }
    }

    /**
     * @brief Clear the object content.
     */
    protected clearImpl(): void
    {
        const self = this;
        const anySelf: any = self;
        const keys = Object.keys(self);
        for (let i = 0; i < keys.length; ++i)
        {
            const key = keys[i];
            delete anySelf[key];
        }
    }

    /**
     * @brief Clone this object.
     * @return {MyJsonMap<Element>} The copy.
     */
    clone(): MyJsonMap<Element>
    {
        const self = this;
        const ret = new MyJsonMap<Element>(
            self._getValidatorElement().clone() as Element,
            self.isMandatory(),
            self.getJsonName()
        );
        self._cloneImpl(ret);
        return ret;
    }

    /**
     * @brief Check if two JSON objects are equals.
     * @param {MyJson} other The other object of comparison.
     * @return {boolean} True if they are equals.
     */
    isEqual(other: MyJson): boolean
    {
        const self = this;
        if (!(other instanceof MyJsonMap)) return false;
        const otherObj = other as MyJsonMap<Element>;
        return self._isEqualImpl(otherObj);
    }

    /**
     * @brief Execute the given callback on each element.
     * The callback takes the key, the element, and the whole object.
     * It returns true to break the loop before having rolled on all elements.
     * @param {(key:string,value:Element,obj:MyJsonMap<Element>)=>boolean} func
     *     The callback.
     * @param {any | null | undefined} [funcThis=null] The optional "this" for
     *     the callback.
     */
    forEach(
        func: (
            key: string,
            value: Element,
            obj: MyJsonMap<Element>
        ) => boolean,
        funcThis: any | null | undefined = null
    ): boolean
    {
        return this._forEachImpl(func as any, funcThis);
    }

    /**
     * @brief Merge the two objects.
     * Already existent keys are merged recursively.
     * @param {MyJson} other The other object to merge.
     * @throws {Error} If the two objects cannot be merged.
     */
    merge(other: MyJson): void
    {
        const self = this;
        if (!(other instanceof MyJsonMap))
        {
            self._throwValidatorError("Invalid other instance.");
        }
        const otherObj = other as MyJsonMap<Element>;
        if (!self._getValidatorElement().isEqual(
            otherObj._getValidatorElement()
        ))
        {
            self._throwValidatorError("Invalid other type instance.");
        }
        self._mergeImpl(otherObj);
    }

    /**
     * @brief Add a new child element.
     * This is a build-time support method.
     * @param {MyJson} _element The child element.
     * @return {MyJson} This.
     * @throws {Error} If element cannot be added.
     */
    add(_element: MyJson): MyJsonMap<Element>
    {
        const self = this;
        self._throwValidatorError("Cannot add elements to MyJsonMap.");
        return self;
    }
}


export {
    MyJsonMap
};

export default MyJsonMap;
