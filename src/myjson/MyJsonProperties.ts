import {MyJson} from "./myjsonImpl";

/**
 * @brief Hidden properties of MyJson.
 */
class MyJsonProperties
{
    /** Whether the object is mandatory. */
    readonly isMandatory: boolean;

    /** Whether the object is initialized. */
    isInitialized: boolean;

    // @todo make it readonly
    /** The name of the object. */
    name: string

    /** The list of constraints. */
    readonly constraints: Array<(ref: MyJson) => string>;


    /**
     * @brief Constructor.
     * @param {boolean} isMandatory True if the object must appear in the
     *     hierarchy.
     * @param {string} name The name of the object.
     */
    constructor(
        isMandatory: boolean,
        name: string
    )
    {
        this.isMandatory = isMandatory;
        this.isInitialized = false;
        this.name = name;
        this.constraints = [];
    }
}

export {
    MyJsonProperties
};

export default MyJsonProperties;
