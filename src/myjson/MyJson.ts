import {MyJsonProperties} from "./myjsonImpl";

/** Key for symbol name. */
const KEY_NAME = "_okmj_impl";


/**
 * @brief Base class for all JSON objects.
 */
abstract class MyJson
{
    /**
     * @brief Constructor.
     * @param {boolean} isMandatory True if the object must appear in the
     *     hierarchy.
     * @param {string} name The name of the object.
     */
    constructor(
        isMandatory: boolean,
        name: string
    )
    {
        const anySelf: any = this;
        const props = new MyJsonProperties(isMandatory, name);
        anySelf[Symbol.for(KEY_NAME)] = props;
    }

    /**
     * @brief Return the internal properties of the object.
     * It uses a symbol to hide the properties to "usual" javascript methods.
     * @private
     * @return {MyJsonProperties} The object properties.
     */
    private _getValidatorProperties(): MyJsonProperties
    {
        const anySelf: any = this;
        return anySelf[Symbol.for(KEY_NAME)];
    }

    /**
     * @brief Throw a validation error.
     * @protected
     * @param {string} msg Te error message.
     * @throws {Error} The error.
     */
    protected _throwValidatorError(msg: string): void
    {
        const self = this;
        const name = self._getValidatorProperties().name;
        let all = msg;
        if (name !== "") all = name + ": " + msg;
        throw new Error(all);
    }

    /**
     * @brief Get if the object is mandatory.
     * @return {boolean} True if it is mandatory.
     */
    isMandatory(): boolean
    {
        return this._getValidatorProperties().isMandatory;
    }

    /**
     * @brief Get if the object has been initialized.
     * @return {boolean} True if initialized.
     */
    isInitialized(): boolean
    {
        return this._getValidatorProperties().isInitialized;
    }


    /**
     * @brief Set if the object has been initialized.
     * @param {boolean} initialized True if initialized.
     */
    protected _setInitialized(initialized: boolean): void
    {
        this._getValidatorProperties().isInitialized = initialized;
    }

    /**
     * @brief Get the name of this json object.
     * @return {string} The name.
     */
    getJsonName(): string
    {
        return this._getValidatorProperties().name;
    }

    /**
     * @brief Get the lidt of constraints.
     * @protected
     * @return {Array<(ref: MyJson) => string>} The list of constraints.
     */
    protected getConstraints(): Array<(ref: MyJson) => string>
    {
        return this._getValidatorProperties().constraints;
    }

    /**
     * @brief Set the name of this json object.
     * @protected
     * @param {string} The name.
     */
    setJsonName(name: string): void
    {
        this._getValidatorProperties().name = name;
    }

    /**
     * @brief Add a constraint which must hold to validate the JSON.
     * The constraint takes a reference to this, and must return an empty string
     * if satisfyed, a message otherwise.
     * @param {foo: (ref: MyJson) => string} foo The constraint.
     */
    addConstraint(foo: (ref: MyJson) => string): void
    {
        const self = this;
        self.getConstraints().push(foo);
    }

    protected checkConstraints(): void
    {
        const self = this;
        function doCheck(constraint: (ref: MyJson) => string): void
        {
            const ret = constraint(self);
            if (ret === "") return;
            self._throwValidatorError(ret);
        }
        self.getConstraints().forEach(doCheck);
    }


    /**
     * @brief Parse a JSON object.
     * @param {any} json The JSON to parse.
     * @throws {Error} If a validation error occurrs.
     */
    parseJson(json: any): void
    {
        const self = this;
        self.clear();
        self._setInitialized(true);
        self.parseJsonImpl(json);
        self.checkConstraints();
    }

    /**
     * @brief Clear the object content.
     */
    clear(): void
    {
        const self = this;
        self._setInitialized(false);
        self.clearImpl();
    }

    /**
     * @brief Actual parse of a JSON object.
     * @param {any} json The JSON to parse.
     * @throws {Error} If a validation error occurrs.
     */
    protected abstract parseJsonImpl(json: any): void;


    /**
     * @brief Clone this object.
     * @return {MyJson} The copy.
     */
    abstract clone(): MyJson;

    /**
     * @brief Check if two JSON objects are equals.
     * @param {MyJson} other The other object of comparison.
     * @return {boolean} True if they are equals.
     */
    abstract isEqual(other: MyJson): boolean;

    /**
     * @brief Actual clear.
     */
    protected abstract clearImpl(): void;

    /**
     * @brief Merge the two objects.
     * @param {MyJson} other The other object to merge.
     * @throws {Error} If the two objects cannot be merged.
     */
    abstract merge(other: MyJson): void;

    /**
     * @brief Add a new child element.
     * This is a build-time support method.
     * @param {MyJson} element The child element.
     * @return {MyJson} This.
     * @throws {Error} If element cannot be added.
     */
    abstract add(element: MyJson): MyJson;
}


export {
    MyJson
};

export default MyJson;
