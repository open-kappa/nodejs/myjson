/**
 * @brief Hidden properties of MyJsonFlex.
 */
class MyJsonFlexProperties<Element>
{
    /** The validator element. */
    element: Element;

    /**
     * @brief Constructor.
     * @param {Element} element The validator element.
     */
    constructor(element: Element)
    {
        this.element = element;
    }
}


export {
    MyJsonFlexProperties
};

export default MyJsonFlexProperties;
